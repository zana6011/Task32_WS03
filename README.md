# Lidar measurements for wake assessment

## Description
In this project a comparative exercise is proposed in order to evaluate 
the possible methods that could be applied to evaluate the characteristics of 
a wind turbine wake from lidar measurements of a shallow conical sector.

For the assignment simulations of lidar measurements in a Large Eddy Simulation (LES)
wind field are provided in the [simulation_data](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/tree/master/simulation_data) folder. 
The simulated measurements intersect the wake of the wind turbine included in the LES.

A detailed description of the assigment is given in the [documentation](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/tree/master/documentation) of the 
project.

This exercise is proposed as preparation for the [IEA Wind Task 32 Workshop #3](http://www.ieawindtask32.org/workshop-3/) which takes place on October 4, 2016.

## Submission of results

The results can be submitted by mail to Davide Trabucchi (davide.trabucchi@uni-oldenburg.de) preferably before September 25, 2016 
and can be presented during the workshop.
 

