# Simulation of lidar measurements in a wind turbine wake

## Large Eddy Simulations
The lidar simulation provided for the comparative exercise was calculated using
a wind field evaluated with PALM, a Large Eddy Simulation (LES) model optimised for parallel computing. The ACtuator Line (ACL) 
approach is also included to simulate the wake of a wind turbine with a 62 m rotor diameter and 61 m hub height. The reference wind field 
simulates the wind vector every 0.4 s for a ten minutes, on a grid with 4 m resolution. The boundary conditions were chosen in order to simulate 
a neutral stratification of the atmosphere, an average inflow wind speed at hub height of 9 m/sand a wind direction of 270° (west). 
The LES domain extends from 8m to 396 m in the stremwise direction, the corrseponding cross-stream axis is defined between 2 and 1026m 
and the vertical axis starts two meters below the ground and reaches 258 m. For convention, the cross-stream axis identifies the North direction.

## Lidar simulations
In the LiDAR simulation, the position of the laser beam was actualized every 0.4 s. For each selected range gate, 15 equally spaced points of 
the corresponding sample volume were interpolated from the wind field and a weighted average of the line-of-sight velocity was applied.
The sample volume was defined on a linear coordinate *s* oriented as the line-of-sight direction and centered on the target point. 
We used a range-gate length corresponding to a spatial extension *∆p* of 36 m and a Gaussian laser pulse with intensity *Ip*, 
characterized by a Full Width Half Maximum *∆r* of 30 m. Finally the radial wind speed v_LOS was calculated as illustrated in the equation 
below by means of a numerical implementation.

<img src=vlos_volume_average.png width=400  />
*Volume average modelling according to Frehlich (2001)*

## Measurement layout
The turbine is positioned in the wind field domain at T=(154,512,0) while the lidar is located at L=(-344.9,213.0,0.8). The objective of the simulated 
measurement is the wake profile at hub height two rotor diameters downstream the turbine.

For this purpose, 30° Plan Position Indicator (PPI) scans at about 5° elevation were simulated with a stop-and-measure approach and 0.5° angular 
resolution. Each scan lasts 23.6 s and includes 46 range gates from 400 m to 850 m in 10 m steps. In total 25 scans were repeated. Consecutive scans are separated by 0.8 s.
For simplicity, and because of the time resolution of the wind field the accumulation time was neglected in the simulation of the radial spead 
measurements.

The figures below describe the layout of the simulation: The red triangle indicates the lidar position, the red transparent 
surface represents the PPI sector. A sketch of the turbine is also included in the figures, and the light blue surface 
shows the position of the corresponding wake. 

Due to the limited extension of the wind field domain in relation to the volume extension considered for the simulation of 
the radial wind speed measurements, not all the measurements included in the PPI scans could be simulated. 
The green points in the top view show the available measurement points. 

### Top view
<img src=top_view_3.png />


### Side view
<img src=side_view.png />

### Front view
<img src=front_view.png width="70%" />

# Files
The lidar simulation are store as comma-separated files in [simulation_data](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/tree/master/simulation_data). 
The files included in this folder are:

- [experimental_setup.dat](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/experimental_setup.dat)
  
    Definition of the lidar position, the turbine position and rotor diameter all expressed in meters.

- [range_gates.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/range_gates.csv)
  
    Sequence of the 46 range gates in meters

- [beam_time_series.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/beam_time_series.csv)
    
    Time series organized in columns as follows:
    
   1. time series of the time stamp of the measurement simulations in seconds
   2. azimuth angle defined in degrees, positive in clockwise direction starting from the North direction, i.e. from the y axis (tolerance=0.1°)
   3. elevation angle defined in degrees, positive in upward direction from the horizontal plane x-y (tolerance=0.1°)

- [x_position.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/x_position.csv), 
[y_position.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/y_position.csv), 
[z_position.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/z_position.csv)

    Coordinates of the measured points. The columns correspond to the range gates, the lines to the measurement time stamps. 
    
- [radial_speed_simulation.csv](https://gitlab.uni-oldenburg.de/zana6011/Task32_WS03/blob/master/simulation_data/radial_speed_simulation.csv)

    Simulated radial measurements (tolerance=0.2 m/s). The columns correspond to the range gates, the lines to the measurement time stamps. 
    *Not a Number* value, i.e. *NaN*, are included for measurement points where the probe volume of the lidar exceeded the LES domain.

# Task

For the provided measurements, evaluate the average horizontal wind speed along with the correspondent 
uncertainty every 10 m from y=419m to y=599m at:

1. x=278m and z=61m (2 rotor diameter downstream)
2. x=216m and z=57m (1 rotor diameter downstream)
2. x=123m and z=50m (0.5 rotor diameter upstream)

For the uncertainty you are invited to combine the results from the two menthods of evaluation Type A and Type B: 
The former is based on statistical analysis, the latter is founded on scientific judgement 
based on all of the available information on the possible variability of the measurements.

The grid of the simulated measurements and the points along the above defined 
lines are visualized in the following picture.

The results for the 19 points at the three specified x,z positions should be provided in separated 
comma-separated files composed by the following lines of data:

1. The horizontal wind speed
2. The uncertainty of the horizontal wind speed

These files can be submitted by mail to Davide Trabucchi (davide.trabucchi@uni-oldenburg.de).

### Grid definition
<img src=grid.png width="40%" />

## References
Frehlich, R., 2001: Estimation of velocity error for doppler lidar measurements. Journal of Atmospheric and Oceanic Technology, 18, 1628–1639, URL [http://journals.ametsoc.org/doi/pdf/10.
4311175/1520-0426%282001%29018%3C1628%3AEOVEFD%3E2.0.CO%3B2](http://journals.ametsoc.org/doi/full/10.1175/1520-0426%282001%29018%3C1628%3AEOVEFD%3E2.0.CO%3B2)
