**Notes**

Not a Number (NaN) entries are included in the radial speed simulation file. 
They occurr at the boundary of the Large Eddy Simulation Domain, where it was
not possible to simulate the lidar volume average.

---------------------

The position files include a scans of 60 entries repeated 25 times.
